import { Form } from 'antd-mini/es/Form/form';
import { myRequest } from '../../utils/http';
const dayjs = require('dayjs')
const app = getApp()
let account = 1;
let account_name = 1;
Page({
  form: new Form({
    rules:{
      name:[{required:true,message:'请输入门店名称'}],
      store_id_value:[{required:true,message:'请输入门店ID'}],
      brand:[{required:true,message:'请选择品牌'}],
      // warehouse_number:[{required:true,message:'请输入仓位数'}],
      name_of_payee:[{required:true,message:'请输入收款人姓名'}],
      phone:[
        {required:true,message:'请输入收款人电话'},
        (form) => ({
          async validator(_, value) {
            let reg = /^1[3456789]\d{9}$/;
            if (reg.test(value)) {
              return
            }
            throw new Error('收款人电话格式错误');
          },
        }) 
      ],
      pay_bank_card:[{required:true,message:'请输入收款卡号'}],
      opening_bank:[{required:true,message:'请输入开户行'}],
      rental:[
        {required:true,message:'请输入预付款金额'},
        (form) => ({
          async validator(_, value) {
            let reg = /^\d+(\.\d{1,2})?$/;
            if (reg.test(value)) {
              return
            }
            throw new Error('预付款金额输入错误');
          },
        })
      ],
      tax_amount:[
        {required:true,message:'请输入含税金额'},
        (form) => ({
          async validator(_, value) {
            let reg = /^\d+(\.\d{1,2})?$/;
            if (reg.test(value)) {
              return
            }
            throw new Error('含税输入错误');
          },
        }) 
      ],
    }
  }),
  data: {
    tableData: [],
    columns: [
      { title: '提成系数', field: 'num1', width: '250' },
      { title: '每天最低回款线', field: 'num2', width: '250' },
      { title: '天数', field: 'num3', width: '250' }
    ],
    headersLen:null,
    tbodyMaxHeight: null,
    list: [{
      id: `store_id${account++}`,
      name:`store_name${account_name++}`
    }],
    brandList: [
      { value: '美团', label: '美团' },
      { value: '街电', label: '街电' },
    ],
    show_name:true,
    fileList0: [],
    fileList1: [],
    fileList2: [],
    fileList3: [],
    fileList4: [],
    fileList5: [],
    images0:[],
    images1:[],
    images2:[],
    images3:[],
    images4:[],
    images5:[],
    width: '',
    height: '',
    edu:0,
    grade:'',
  },
  onLoad(e){},
  iptChange(value, e) {
    let reg = /^\d+(\.\d{1,2})?$/;
    if (!reg.test(value)) {
      this.setData({
        tableData: []
      })
      return
    }

    let tableData = []
    let obj = {
      90: '25%',
      180: '20%',
      270: '10%',
      360: '0%',
    }
    for(let i in obj){
      tableData.push({'num1': obj[i],'num2': (Number(value)/i).toFixed(2),'num3': i})
    }

    this.setData({
      tableData
    })
  },
  uploadChange(fileList) {
    // 这里的数据包括上传失败和成功的图片列表，如果需要筛选出上传成功的图片需要在此处理
    console.log('图片列表：', fileList);
  },
  // 上传图片
  uploadImage(e) {
    let that = this,type = e.target.dataset.type;

    if(type === '1' || type === '2' || type === '3' || type === '4' || type === '5'){
      let fileList = that.data['fileList' + type]
      if(fileList.length > 0){
        my.showToast({ content: '只能上传一张图片', duration: 1000 });
        return
      }
    }

    dd.chooseImage({ 
      sourceType: ['camera', 'album'],
      count: 1,
      success: (res1) => {
        console.log('res1', res1);
        dd.showLoading({
          content: '转码中...'
        });
        dd.compressImage({
          filePaths: res1.filePaths,
          compressLevel: 2,
          success: (res2) => {
            console.log(res2)
            // 获取图片信息
            dd.getImageInfo({
              src: res2.filePaths[0],
              success: (res) => {
                console.log(res.width, 'res')
                const { width, height, path } = res;
                that.setData({
                  width,
                  height
                });
                const ctx = dd.createCanvasContext('canvas' + type);
                ctx.drawImage(path, 0, 0, width, height);
                ctx.draw(true, () => {
                  // 获取画布指定区域的 data URL 数据
                  ctx.toDataURL({
                    x: 0,
                    y: 0,
                    width: width,
                    height: height,
                    destWidth: width,
                    destHeight: height,
                  }).then(dataURL => {
                    console.log(dataURL)  // 获取base64数据
                    return dataURL
                  }).then(res => {
                    dd.hideLoading();
                    that.onUpload(res,type,width,height); // 将base64数据传给后台的方法
                  })
                })
              }
            })
          }
        });
      },
      fail: (e) => {
        console.log('fail----------------------')
      }
    })
  },
  async onUpload(file,type,width,height) {
    let that = this

    let param = {
      images: file
    }

    let res = await myRequest('/advance-api/upload','POST',param)
    if(res.errcode === 0){
      let fileList = that.data['fileList' + type],images= that.data['images' + type]
      fileList.push({
        url: file,
        width: width,
        height: height
      })
      images.push(res.data)

      let obj = {}
      obj['fileList' + type] = fileList
      obj['images' + type] = images
      
      that.setData(obj)
    }else{
      my.showToast({ content: '上传图片失败', duration: 1000 });
    }
  },
  handleRef(ref) {
    this.form.addItem(ref);
  },
  reset() {
    this.form.reset();
  },
  add() {
    this.setData({
      list: [
        ... this.data.list,
        {
          id: `store_id${account++}`,
          name:`store_name${account_name++}`
        },
      ],
      show_name:false
    });
  },
  minus(e) {
    const { index } = e.currentTarget.dataset;
    const list = [...this.data.list];
    list.splice(index, 1);
    this.setData({
      list,
      show_name:list.length == 1 ? true : false
    });
  },
  async submit() {
    let that = this

    const values = await this.form.submit();
    let param = {
      store_id_value: values.store_id_value
    }
    // let chazhi = values.tax_amount - values.rental
    
    // if(values.rental > that.data.edu){
    //   my.showToast({ content: '您的等级为' + that.data.grade + ',最高申请额度为'+that.data.edu, duration: 1000 });
    //   return
    // }

    // for (const key in values) {
    //   if (key.startsWith("store_id")) {
    //     const storeId = values[key];
    //     const storeNumber = key.replace("store_id", "");
    //     const storeNameKey = `store_name${storeNumber}`;
    //     const storeName = values[storeNameKey];
    
    //     param.store_id_json.push({
    //       store_id: storeId,
    //       store_name: storeName
    //     });
    //   }else if(!key.startsWith("store_name")){
    //     param[key] = values[key]
    //   }
    // }

    // let images0 = this.data.images0
    // if(images0.length === 0){
    //   my.showToast({ content: '请上传合同图片', duration: 1000 });
    //   return
    // }

    // param.images = images0

    let res = await myRequest('/store-api/direct-create','POST',param)
    if (res.errcode != 0) {
      dd.showToast({
        type: 'none',
        content: res.errmsg,
        duration: 2000
      });
      return
    }

    dd.showToast({
      type: 'none',
      content: '创建成功',
      duration: 2000
    });
    that.reset()
    dd.reLaunch({
      url: '/pages/for_rent/for_rent'
    })
  }
});