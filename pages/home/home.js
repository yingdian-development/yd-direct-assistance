const app = getApp()
import { myRequest } from '../../utils/http';
Page({
  data: {
    tableData: [],
    columns: [      
      { title: '门店名称', field: 'store_name', width: '350',fixed:'left',distance:'0' },
      { title: '门店ID', field: 'store_id_value', width: '350' }
    ],
    headersLen:700,
    tbodyMaxHeight: null,
    role_slug:'',
    userid: '',
    page: 1,
    limit:50,
    count: 0,
    nextStatus: null,//1有下一页 ，0没有下一页
    sort_field: '',
    asc_desc: '',//升序asc 降序desc
    daily_average_list: [
      { label: '小于1', value: 1},
      { label: '小于3', value: 3},
      { label: '小于5', value: 5},
      { label: '小于7', value: 7},
      { label: '小于9', value: 9},
      { label: '小于11', value: 11},
      { label: '小于13', value: 13}
    ],
    regional_director_list: [],
    region_list: [],
    person_list: [],
    city_list:[],
    month_level_list:[
      { label: 'A', value: 'A'},
      { label: 'B', value: 'B'},
      { label: 'C', value: 'C'},
      { label: 'D', value: 'D'},
      { label: 'E', value: 'E'},
      { label: 'F', value: 'F'},
      { label: 'G', value: 'G'},
      { label: '撤机', value: '撤机'}
    ],
    topList: []
  },
  onShow() {
    let that = this,role_slug = app.userData.data.role_slug
    dd.getSystemInfo({
      success: function (res) {
        let clientHeight = res.windowHeight;// 获取可使用窗口宽度
        let clientWidth = res.windowWidth;// 获取可使用窗口高度
        let ratio = 750 / clientWidth;// 算出比例
        let height = clientHeight * ratio;// 算出高度(单位rpx)

        let tbodyMaxHeight = height - 100
        if(role_slug === 'region' || role_slug === 'regional_director'){//大区
          tbodyMaxHeight = height - 150
        }
        // if(role_slug === 'chairman' || role_slug === 'chairman_assistant' || role_slug === 'generat_manager'){//董事长
        //   tbodyMaxHeight = height - 640
        // }else if(role_slug === 'regional_director'){//大区总
        //   tbodyMaxHeight = height - 430
        // }else if(role_slug === 'region'){//大区
        //   tbodyMaxHeight = height - 350
        // }else if(role_slug === 'bd'){//bd
          
        // }

        // 设置高度
        that.setData({
          tbodyMaxHeight: tbodyMaxHeight
        });
      }
    })
    app.authLogin(function() {
      that.setData({
        role_slug: role_slug,
        userid: app.userData.data.userid
      })

      // if(role_slug === 'chairman' || role_slug === 'chairman_assistant' || role_slug === 'generat_manager'){//董事长
      //   that.initPicker('regional_director','')
      //   that.initPicker('region','')
      //   that.initPicker('bd','')
      // }else if(role_slug === 'regional_director' || app.userData.data.userid === '16866369325604298' || app.userData.data.userid === '16968429687969801'){//大区总
      //   that.initPicker('region','')
      //   that.initPicker('bd','')
      // }else if(role_slug === 'region'){//大区
      //   that.initPicker('bd','')
      // }
      that.getList()
    })
  },
  // 下拉框事件
  handleOk(value, column, e) {
    let type = e.target.dataset.type
    if(type === '1'){//团队
      app.regional_director = value
      app.region = ''
      app.person = ''

      this.initPicker('region',value)
      this.initPicker('bd','')
    }else if(type === '2'){//组别
      app.region = value
      app.person = ''

      this.initPicker('bd',value)
    }else if(type === '3'){//归属人
      app.person = value
    }else if(type === '4'){//城市
      app.city = value
    }else if(type === '5'){//门店等级
      app.month_level = value
    }
    //重置页码
    this.setData({page:1})
    this.getList()
  },
  // 初始化团队、组别、归属人的下拉值
  async initPicker(type,id){
    let that = this,role_slug = this.data.role_slug,obj = {};

    obj = {
      type: type,
      id: id
    }

    let res = await myRequest('/api/get-select-user','GET',obj)

    if(res.errcode === 0){
      let itemList = [{ label: '全部', value: ''}];

      let list = res.data
      for(let i = 0;i< list.length;i++){
        itemList.push({ label: list[i].label, value: list[i].value})
      }

      if(type === 'regional_director'){
        that.setData({
          regional_director_list:itemList
        })
      }else if(type === 'region'){
        that.setData({
          region_list:itemList
        })
      }else if(type === 'bd'){
        that.setData({
          person_list:itemList
        })
      }
    }
  },
  // 触发到表格顶部
  upTable(e){
    if(this.data.page != 1){
      this.setData({
        page:this.data.page - 1
      })
  
      this.getList()
    }
  },
  // 触发到表格底部
  upperTable(e) {
    if(this.data.nextStatus === 1){
      this.setData({
        page:this.data.page + 1
      })
  
      this.getList()
    }
  },
  // 对指定的field进行升序或者降序
  upOrDown(field){
    let arr = this.data.columns
    arr.filter((i) => {
      if(i.field === field){
        this.setData({
          sort_field: field,
          asc_desc: i.upOrDown ? 'asc' : 'desc'//true是up false是down
        })

        i.upOrDown = !i.upOrDown
      }
    });

    this.setData({
      columns: arr
    })

    this.getList()
  },
  //获取列表
  async getList() {
    let that = this

    let res = await myRequest('/store-api/direct-list','GET',{})

    that.setData({
      tableData:res.data || [],
    })
    if (res.errcode != 0) {
      dd.showToast({
        type: 'none',
        content: res.data.errmsg,
        duration: 2000
      });
    }
  },
  handleTapApproval(e) {
    // 带参数的跳转，从 page/index 的 onLoad 函数的 query 中读取 xx
    dd.navigateTo({ url: '/pages/approveList/approveList'})
  },
  onConfirm(value) {
    app.daily_average = value
    this.getList()
  },
  onChange(value) {
    app.daily_average = value
  },
  // 跳转页面
  dumpTab(e) {
    let type = e.target.dataset.type,url = '';
    console.log(type)
    if(type === '1'){//新增直助代
      url = '../form/form?type=1'
    }

    if(!url) return;
    dd.navigateTo({
      url: url
    });
  }
});