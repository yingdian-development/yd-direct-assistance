Component({
  data: {},
  props: {
    tableData: [],
    columns: [],
    headersLen: '',//表头数量
    tbodyMaxHeight: '',//表格body最大高度
    isPage: false
  },
  didMount() {},
  didUpdate() {},
  didUnmount() {},
  methods: {
    // 滚动到最下面时触发
    upper(e) {
      if(this.props.isPage){
        this.props.onUpperView(e)
      }
    },
    // 滚动到最上面时触发
    upward(e){
      if(this.props.isPage){
        this.props.onUpView(e)
      }
    },
    // up升序 down降序
    upOrDown(e){
      let field = e.target.dataset.field,isSort = e.target.dataset.isSort
      if(isSort){
        this.props.onUpOrDown(field)
      }
    }
  }
});