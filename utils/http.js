import qs from 'qs'
const app = getApp()

export async function myRequest(uri, method = 'GET', data = {}, config = {}) {
  // 未传入headers 
  let headers = {}
  if (method == 'GET') {
      data = qs.stringify(data, { arrayFormat: 'indices' })
  }else{
    Object.assign(headers, {
      'content-type': 'application/json'
    })
    data = JSON.stringify(data)
  }
  let token = dd.getStorageSync({ key: 'token' }).data
  let api_url = app.api + uri

  return new Promise((resolve, reject) => {
    dd.httpRequest({
      headers: Object.assign(headers, {
        'X-Token': token || ''
      }),
      url: api_url,
      data: data,
      method: method,
      ...config,
      success: (res) => {
        if (res.data.errcode != 0) {
          dd.showToast({
            type: 'exception',
            content: res.data.message,
            duration: 2000
          });
        }
        resolve(res.data)
      },
      fail: (res) => {
        dd.showToast({
          type: 'exception',
          content: '网络异常',
          duration: 2000
        });
        reject(res)
      }
    })
  })
}


